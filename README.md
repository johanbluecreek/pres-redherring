# Anti-D6-brane singularities and resolution

## Abstract

Recently anti-brane singularities have been argued to be resolved when the
polarisation have been taken into account properly. The anti-D6-brane
singularity however seems to be the odd-one out. In this talk I will present
recent work that suggests that the anti-D6-brane can be resolved in the same
way by avoiding previous no-gos and give numerical evidence that these
solutions could be found in supergravity as well.

## Information

Talk is based mainly on the paper

 * https://arxiv.org/abs/1907.05295

Talk was given at

 * The sixth Meeting of the INFN Networks GAST, GSS and ST&FI, devoted to "Theories of the Fundamental Interactions", https://agenda.infn.it/event/20096/overview, @ 2019-10-21 (see tag Torino-20191021)
 * Invited seminar at Swansea University, @ 2019-11-22 (see tag Swansea-20191122)
